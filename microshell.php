#!/usr/bin/php
<?php
// microshell.php for microshell.php in /Users/yohanpiron/ETNA/PHP/microshell
//
// Made by yohan piron
// Login   <piron_y@etna-alternance.net>
//
// Started on  Fri Apr 11 23:28:22 2014 yohan piron
// Last update Sat Apr 12 18:25:37 2014 yohan piron
//
include_once ("./include/functions_1.php");
include_once ("./include/functions_2.php");
include_once ("./include/prompt.php");
include_once ("./include/tools_1.php");
include_once ("./include/tools_2.php");
include_once ("./include/tools_3.php");

$my_path = dirname(__FILE__);
$my_previous_path = "";
prompt();