<?php

// Calculates string length
function my_strlen($str)
{
  for ($i = 0; isset($str[$i]); $i++);
  return ($i);
}

// Calculates array length
function my_arraylen($str)
{
  $total = 0;
  foreach ($str as $key => $value)
    {
      $total++;
    }
  return ($total);
}

// Checks if there's a whitespace in a string
function my_spacecheck($len, $str)
{
  for ($i = 0; $i < $len; $i++)
    {
      if ($str[$i] == " ")
	{
	  $bool = true;
	  break;
	}
      else
	$bool = false;
    }
  return ($bool);
}

// If there's a whitespace in user input, explodes the content in a array
function explode_params($content)
{
  if (my_spacecheck(my_strlen($content), $content) == true)
    {
      $params = explode(" ", $content);
      return ($params);
    }
  else
    return ($content);
}