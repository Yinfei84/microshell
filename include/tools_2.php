<?php
// Tries to read file
function process_file($file)
{
  if (!file_exists($file))
    echo ("\033[31m".'cat: '.$file.": No such file or directory"."\033[37m"."\n");
  else if (is_dir($file))
    echo ("\033[31m".'cat: '.$file."/: Is a directory"."\033[37m"."\n");
  else
    {
      if (is_readable($file))
	{
	  $open_file = fopen($file, 'r');
	  if ($open_file == false)
	    {
	      echo ("\033[31m".'cat: '.$file.": Cannot open file"."\033[37m"."\n");
	      return(false);
	    }
	  $read_file = fread($open_file, filesize($file));
	  echo $read_file;
	  fclose($open_file);
	}
      else
	echo ("\033[31m".'cat: '.$file.": Permission denied"."\033[37m"."\n");
    }
}

// "cd -" tool
function my_swap($my_path, $my_previous_path)
{
  global $my_path;
  global $my_previous_path;

  $tmp = $my_path;
  $my_path = $my_previous_path;
  $my_previous_path = $tmp;
}

// Updates current path and saves the previous one
function path_update($new_path)
{
  global $my_path;
  global $my_previous_path;

  $my_previous_path = $my_path;
  $my_path = $new_path;
}

// Print available results for ls
function show_ls_files($directory)
{
  while (($file = readdir($directory)) !== false)
    print_ls_result($file);
  closedir($directory);
}

// Puts effetcs on ls shown files like "/", "@"...
function print_ls_result($str)
{
  if ($str[0] == ".")
    return (0);
  else if (is_dir($str))
    echo $str."/\n";
  else if (is_executable($str))
    echo $str."*\n";
  else if (is_link($str))
    echo $str."@\n";
  else
    echo $str."\n";
}