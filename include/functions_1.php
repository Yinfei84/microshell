<?php
// PWD (OK) - TOUCH (OK) - LS - ECHO (OK)

function my_pwd()
{
  global $my_path;
  echo ($my_path."\n");
}

function my_touch($str)
{
  foreach ($str as $key => $value)
    {
      if ($key > 0)
	{
	  $file = fopen($value, 'w');
	  fclose($file);
	}
    }
}

function my_ls($dir)
{
  global $my_path;
  if (!is_array($dir))
    {
      if (!file_exists($dir[1]))
	$directory = opendir($my_path);
      if ($directory !== false)
	show_ls_files($directory);
    }
  else
    {
      if (!file_exists($dir[1]))
	echo ("\033[31mls: No such file or directory"."\033[37m"."\n");
      else
	{
	  $search = array();
	  for ($i = 1; isset($dir[$i]); $i++)
	    $search[$i - 1] = $dir[$i];
	  foreach ($search as $dir)
	    {
	      $directory = opendir($dir);
	      if ($directory !== false)
		show_ls_files($directory);
	    }
	}
    }
}

function my_echo($str = null)
{
  if ($str == "echo")
    echo ("\n");
  else
    {
      $array_len = my_arraylen($str);

      foreach ($str as $key => $value)
	{
	  if ($key > 0)
	    echo $value;
	  if (($key > 0) && ($key < $array_len))
	    echo " ";
	}
      echo "\n";
    }
}