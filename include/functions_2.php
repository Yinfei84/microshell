<?php
// EXIT (OK) - CAT (OK) - CD (OK)

function my_exit()
{
  die();
}

function my_cat($str)
{
  if (is_array($str))
    $array_len = my_arraylen($str);

  if ($str == 'cat')
    echo ("\033[31m"."cat: Invalid arguments\033[37m\n");
  else
    {
      $files = array();

      for ($i = 1; isset($str[$i]); $i++)
	$files[$i - 1] = $str[$i];

      foreach ($files as $key=>$file)
	{
	  process_file($file);
	  if (($key > 0) && ($key < $array_len))
	    echo ("\n");
	}
    }
}

function my_cd($dir)
{
  global $my_path;
  global $my_previous_path;
  if ($dir[1] == "-")
    my_swap($my_path, $my_previous_path);
  else if ($dir == "cd")
    path_update(getenv("HOME"));
  else
    {
      if (file_exists($dir[1]) && $dir[1] != ".")
	{
	  if ($dir[1] == "..")
	    path_update(dirname($my_path));
	  else
	    path_update($dir[1]);
	}
      elseif (file_exists($my_path."/".$dir[1]) && $dir[1] != ".")
	path_update($my_path."/".$dir[1]);
      else if ($dir[1] == "~")
	path_update(getenv("HOME"));
      else if ($dir[1] == ".")
	return (0);
      else
	echo ("\033[31m"."cd: ".$dir[1].": No such file or directory\033[37m\n");
    }
}

function my_env()
{
  $home_vars = $_ENV;
  $test = var_dump($home_vars);
  foreach ($test as $key => $value)
    {
      echo ($key."=".$value);
    }

}