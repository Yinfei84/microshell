<?php

function prompt()
{
  $func_name = '';
  $cleaned_content = '';
  $flux = fopen("php://stdin", "r");
  if ($flux !== false)
    {
      call_prompt();
      while (($content = fgets($flux)) !== false)
	{
	  while ($content == "\n")
	    prompt();
	  $cleaned_content = clean_params($content);
	  $params = explode_params($cleaned_content);
	  $func_name = ((is_array($params)) ? "my_".$params[0] : "my_".$params);
	  if(function_exists($func_name))
	    $func_name($params);
	  else
	    command_not_found($params);
	  $funct_name = '';
	  $cleaned_content = '';
	  call_prompt();
	}
    }
}

function call_prompt()
{
  echo "\033[32m$> \033[37m";
}