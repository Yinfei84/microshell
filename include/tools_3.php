<?php
// clean user input form " and "\n"
function clean_params($content)
{
  $cleaned_content = "";
  for ($i = 0; isset($content[$i]); $i++)
    {
      if (isset($content[$i + 1]) && $content[$i + 1] != null
	  && $content[$i] != "\"")
	$cleaned_content .= $content[$i];
    }
  return ($cleaned_content);
}

// "command not" found input
function command_not_found($params)
{
  if (is_array($params))
    echo "\033[31m".$params[0].": Command not found\n";
  else
    echo "\033[31m".$params.": Command not found\n";
}